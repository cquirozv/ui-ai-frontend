import { Component, OnInit } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

import { FormGroup, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit {
  public message='';
  public sentiment='';

  profileForm = new FormGroup({
    text: new FormControl(''),
  });
  constructor(
    private http:HttpClient,
    private spinner:NgxSpinnerService
    ) { }

  ngOnInit(): void {
  }

  textUpload(){
    this.spinner.show();

    this.http.post(environment.TEXT_URL,this.profileForm.value)
    .subscribe((data:any) => {
      var success = data.success;
      
      if (success == true) {
        this.message = data.message.toString();
        this.sentiment = data.sentiment.toString();
        this.profileForm.reset();
        this.spinner.hide();
      }else{
        console.log(data.message);
        this.spinner.hide();
      }
    });
  }

}
