import { Component, OnInit } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

import { FormGroup, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-speech',
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.css']
})
export class SpeechComponent implements OnInit {

  public message='';
  public sentiment='';

  profileForm = new FormGroup({
    audio: new FormControl(''),
  });

  constructor(
    private http:HttpClient,
    private spinner:NgxSpinnerService
    ) {  }

  ngOnInit(): void {
    //this.audioUpload;
  }

  audioUpload(event:any){
    this.spinner.show();
    var file:File=event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('file',file,file.name);

    this.http.post(environment.SPEECH_URL,formData)
    .subscribe((data:any) => {
      var success = data.success;
      
      if (success == true) {
        this.message = data.message.toString();
        this.sentiment = data.sentiment.toString();
        this.profileForm.reset();
        this.spinner.hide();
      }else{
        console.log(data.message);
        this.spinner.hide();
      }
      

    });
  }

}
