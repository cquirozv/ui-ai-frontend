import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SpeechComponent } from './speech/speech.component';
import { TextComponent } from './text/text.component';

const routes: Routes = [
  {path:'speech', component:SpeechComponent},
  {path:'text', component:TextComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
